# Hunger Games Simulator
This repository contains the source code for the Agma Schwa Hunger
Games Simulator: https://www.nguh.org/tools/hunger_games_simulator.

It also includes a [simple webserver](run.js) that may be used to run
the simulator.

## Bug Reports and Feature Requests
For information on how to report bugs and make feature requests, go 
[here](https://www.nguh.org/tools/hunger_games_simulator#source-bugs-contributing)


## Running the Simulator
**N O T E:** Since nguh.org is hosted on a Linux server and mainly developed
by someone whose primary operating system is Linux, the Simulator
officially only runs on Linux; it probably also runs on other OSs, but
there are no plans to test it on or support any platforms other than
Linux.

Running the simulator requires you to have nodejs installed.

Before running the simulator for the first time, run
```bash
$ node run.js --init
```

Before starting the simulator, you will have to compile the `sass` and `ts`
files in `common` and `apps/hgs`. nguh.org has its own build system, which 
is why this repository currently does not include any npm scripts or anything
like that for compiling SASS or TypeScript (save for a 
[tsconfig.json](tsconfig.json)).

You can then start the simulator by running
```bash
$ node run.js --port 20005
```
The `--port` option is optional. The default port is `24005`.

## Contributing
If you want to contribute to the Simulator, you can do so by opening
a pull request on GitLab.

Make sure not to move around any preexisting files since nguh.org
makes assumptions as to where those files are located, and moving
them to a different location would most likely break the entire thing.


The `common` directory in the project root is not a part of this repository;
it's actually a separate repository which you can find 
[here](https://gitlab.com/agma-schwa-public/nguh.org-frontend).
